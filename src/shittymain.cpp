#include "game.hpp"

#include <iostream>

int main()
{
    Game g;
    Player p1 = Player("Player 1");
    Player p2 = Player("Player 2");

    g.add(p1);
    g.add(p2);

    g.play();

    std::cout << g.losers()[0].name << " loses!" << std::endl;

    return 0;
}

