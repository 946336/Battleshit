#include "tracking_grid.hpp"
#include "common.hpp"

#include <algorithm>
#include <iostream>
#include <string>

#define ROW first
#define COL second

Tracking_Grid::Tracking_Grid(const Dims dims)
    : rows(dims.ROW), cols(dims.COL)
{
    /* cells = std::vector< std::vector<Observation> >(dims.ROW, */
    /*         std::vector<Observation>(dims.COL), Observation::Nothing); */
    cells.reserve(dims.ROW);
    for (size_t i = 0; i < dims.COL; ++i) {
        cells.push_back(
            std::vector< Observation >(dims.COL, Observation::Nothing)
        );
    }
}

Tracking_Grid::~Tracking_Grid()
{
    // Nothing to do here
}

bool Tracking_Grid::in_bounds(const Where where)
{
    if (where.ROW >= rows) return false;
    if (where.COL >= cols) return false;
    return true;
}

void Tracking_Grid::clear(const Where tl, const Where br)
{
    size_t top = std::min(tl.ROW, br.ROW);
    size_t bot = std::max(tl.ROW, br.ROW);

    size_t left  = std::min(tl.COL, br.COL);
    size_t right = std::max(tl.COL, br.COL);

    for (size_t row = top; row < bot; ++row) {
        for (size_t col = left; col < right; ++col) {
            cells[row][col] = Observation::Nothing;
        }
    }
}

void Tracking_Grid::record(const Observation shot,
        const Where where)
{
    if (!in_bounds(where)) {
        throw WhereWasThatGoing(
              "No location ("
            + std::to_string(where.ROW)
            + ", "
            + std::to_string(where.COL)
            + ") on a Tracking Grid with dimensions ("
            + std::to_string(rows)
            + ", "
            + std::to_string(cols)
            + ")."
        );
    }

    cells[where.ROW][where.COL] = shot;
}

void Tracking_Grid::record(const Result result)
{
    record(result.observation, result.instigator.to_shoot);
}

void Tracking_Grid::print() const
{
    char label = 'A';
    char vis;
    std::cout << "  0 1 2 3 4 5 6 7 8 9" << std::endl;
    for (const auto &row : cells) {
        std::cout << label << ' ';
        for (const Observation &obs: row) {
            switch (obs) {
                case Observation::Miss:
                    vis = 'O';
                    break;
                case Observation::Hit:
                    vis = 'X';
                    break;
                case Observation::Nothing:
                    vis = ' ';
                    break;
            }
            std::cout << vis << ' ';
        }
        std::cout << label++ << ' ';
        std::cout << std::endl;
    }
    std::cout << "  0 1 2 3 4 5 6 7 8 9" << std::endl;
}

