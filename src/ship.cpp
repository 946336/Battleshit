#include "ship.hpp"

#include <string>
#include <algorithm>
#include <iostream>

static size_t __id = 1;

Sea Open_Sea(std::string("Open Sea"), (size_t) 1);

Ship::Ship()
    : title_("Default constructed ship"), id_(0),
      sections(1, Ship::Damage::Undamaged)
{
    // Nothing to do here
}

Ship::Ship(const std::string name, const size_t length)
    : title_(name), id_(__id++), sections(length, Ship::Damage::Undamaged)
{
    // Nothing to do here
}

Ship::Ship(const std::pair<std::string, size_t> ship_specs)
    : Ship(ship_specs.first, ship_specs.second)
{
    // Nothing to do here
}

Ship::~Ship()
{
    // Nothing to do here
}

std::string Ship::title() const
{
    return title_;
}

size_t Ship::id() const
{
    return id_;
}

Ship& Ship::operator=(const Ship &src)
{
    if (&src == this) {
        return *this;
    }

    title_ = src.title_;
    id_ = src.id_;
    sections = src.sections;

    return *this;
}

bool Ship::operator==(const Ship &other)
{
    return id_ == other.id_;
}

bool Ship::operator!=(const Ship &other)
{
    return !(*this == other);
}

Ship::Damage Ship::take_damage(Ship::Place where)
{
    if (sections.size() <= where) {
        throw WhoDoYouThinkIAm("Cannot damage section "
                + std::to_string(where)
                + " on a ship of length "
                + std::to_string(sections.size()));
    }

    switch (sections[where]) {
        case Ship::Damage::Hit:
            sections[where] = Ship::Damage::Hit;
            break;
        case Ship::Damage::Undamaged:
            sections[where] = Ship::Damage::Hit;
            break;
    }

    return sections[where];
}

Ship::State Ship::state() const
{
    // No undamaged spots -> Sunk
    if (std::find(sections.begin(), sections.end(), Ship::Damage::Undamaged)
            == sections.end()) {
        return Ship::State::Sunk;
    }

    // At least one damaged section -> Damaged ship
    if (std::find(sections.begin(), sections.end(),
                Ship::Damage::Hit) != sections.end()) {
        return Ship::State::Damaged;
    }

    // None of the above -> Pristine Ship
    return Ship::State::Pristine;
}

size_t Ship::length() const
{
    return sections.size();
}

Ship::Damage Ship::part(Ship::Place place) const
{
    if (sections.size() <= place) {
        throw WhoDoYouThinkIAm("Cannot query section "
                + std::to_string(place)
                + " on a ship of length "
                + std::to_string(sections.size()));
    }

    return sections[place];
}

char Ship::ascii() const
{
    return 'S';
}

// ====================================================================
// Open Sea
// ====================================================================

Ship::Damage Sea::take_damage(const Ship::Place where)
{
    if (sections.size() <= where) {
        throw WhoDoYouThinkIAm("Cannot damage section "
                + std::to_string(where)
                + " on a ship of length "
                + std::to_string(sections.size()));
    }

    return Ship::Damage::Undamaged;
}

char Sea::ascii() const
{
    return 'O';
}

