// This is also a shitty example. Don't ever do what make_decision and
// place_ship do, and don't use them as is.

#include "player.hpp"

#include <utility>
#include <iostream>
#include <string>

#include <algorithm>
#include <functional>
#include <cctype>
#include <locale>

// strip from start
static inline std::string &lstrip(std::string &s) {
    s.erase(s.begin(), std::find_if(s.begin(), s.end(),
            std::not1(std::ptr_fun<int, int>(std::isspace))));
    return s;
}

// strip from end
static inline std::string &rstrip(std::string &s) {
    s.erase(std::find_if(s.rbegin(), s.rend(),
            std::not1(std::ptr_fun<int, int>(std::isspace))).base(), s.end());
    return s;
}

// strip from both ends
static inline std::string &strip(std::string &s) {
    return lstrip(rstrip(s));
}

Player::Player(const std::string name_)
    : name(name_)
{
    // Nothing to do here
}

Player::~Player()
{
    // Nothing to do here
}

bool is_valid_coord(std::string &str)
{
    strip(str);

    if (str.length() < 2) return false;

    char &row = str[0];
    char &col = str[1];

    row = toupper(row);

    if (row - 'A' < 0 or row - 'A' > 10 - 1) return false;
    if (col - '0' < 0 or col - '0' > 10 - 1) return false;

    return true;
}

bool is_valid_direction(std::string&str)
{
    strip(str);
    if (str.length() < 1) return false;

    str[0] = toupper(str[0]);

    switch (str[0]) {
        case 'U':
        case 'R':
        case 'D':
        case 'L':
            return true;
        default:
            return false;
    }
}

bool Player::place_ship(const Ship ship)
{
    // For example

    std::string input = "";

    while(!is_valid_coord(input)){
        std::cout << "Your board:" << std::endl;
        board.print();
        std::cout << "Where would you like to place your " << ship.title()
                  << "?" << std::endl;
        std::cout << ">>> ";
        if (!std::getline(std::cin, input)) {
            throw std::runtime_error("You're done already?");
        }
    }

    size_t row = input[0] - 'A';
    size_t col = input[1] - '0';

    Where where = std::make_pair(row, col);

    while(!is_valid_direction(input)) {
        std::cout << "What direction would you like your "
                  << ship.title() << " to face?" << std::endl;
        std::cout << ">>> ";
        if (!std::getline(std::cin, input)) {
            throw std::runtime_error("You're done already?");
        }
    }

    Board::Facing direction;

    switch (input[0]) {
        case 'U':
            direction = Board::Facing::Up;
            break;
        case 'R':
            direction = Board::Facing::Right;
            break;
        case 'D':
            direction = Board::Facing::Down;
            break;
        case 'L':
            direction = Board::Facing::Left;
            break;
    }

    return board.place_ship(ship, where, direction);
}

Turn Player::make_decision()
{
    // Uhh...
    std::cout << name << "'s board:" << std::endl;
    board.print();

    std::cout << name << "'s tracking grid:" << std::endl;
    tracking_grid.print();

    std::string input = "";

    while(!is_valid_coord(input)){
        std::cout << "Where would you like to shoot?" << std::endl;
        std::cout << ">>> ";
        if (!std::getline(std::cin, input)) {
            throw ComeBackHereDammit(
                "Failed to extract attack coordinates from player"
            );
        }
    }

    size_t row = input[0] - 'A';
    size_t col = input[1] - '0';

    Where where = std::make_pair(row, col);

    return Turn(where);
}

Result Player::process(const Turn turn)
{
    return Result(turn, board.shoot(turn.to_shoot));
}

void Player::acknowledge(const Result result)
{
    Result r = result;

    std::string res = (r.observation == Observation::Hit) ? "Hit" : "Miss";

    std::cout << "Result: " << res << std::endl;

    tracking_grid.record(result);
}

bool Player::has_lost() const
{
    return !board.has_unsunk_ships();
}

