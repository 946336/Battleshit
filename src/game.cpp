#include <memory>
#include <iostream>

#include "game.hpp"

static size_t __id = 0;

std::vector< std::pair<std::string, size_t> > Game::ship_types = {
    std::make_pair("Aircraft Carrier", 5),
    std::make_pair("Battleship", 4),
    std::make_pair("Cruiser", 3),
    std::make_pair("Submarine", 3),
    std::make_pair("Patrol Boat", 2),
};

static void clear_screen()
{
    // VT100 escape seqs (clear screen, move hursor to cell 1, 1)
    std::cout << "\033[2J\033[1;1H";
}

Game::Game()
    : id(__id++)
{
    // Nothing to do here
}

Game::Game(Player player1)
    : id(__id++)
{
    players.push_back(player1);
}

Game::~Game()
{
    // Nothing to do here
}

Game::Join Game::add(Player player)
{
    if (players.size() < MAX_PLAYERS) {
        players.push_back(player);
        return Game::Join::Success;
    } else {
        return Game::Join::Full;
    }
}

bool Game::is_over() const
{
    for (const auto p : players) {
        if (p.has_lost()) return true;
    }

    return false;
}

std::vector<Player> Game::losers() const
{
    std::vector<Player> unlucky_people;

    for (auto p : players) {
        if (p.has_lost()) {
            unlucky_people.push_back(p);
        }
    }

    if (unlucky_people.empty()) {
        throw DontCountYourChickensBeforeTheyHatch(
            "The game is not yet over"
        );
    }

    return unlucky_people;
}

Game::Outcome Game::play()
{
    if (players.size() != 2) {
        throw NotSoFast(
              "Cannot start game with only "
            + std::to_string(players.size())
            + " players. Please wait until the game has at least "
            + std::to_string(MIN_PLAYERS)
            + " players and at most "
            + std::to_string(MAX_PLAYERS)
        );
    }

    Player *shooting = &(players[0]);
    Player *receiving = &(players[1]);
    Player *tmp;

    // Both players place ships
    for (auto &p : players) {
        clear_screen();
        std::cout << p.name << " is now placing ships" << std::endl;
        for (auto sp : ship_types) {
            while(!p.place_ship(Ship(sp)));
            clear_screen();
        }
    }

    // Play
    try {
        while(!is_over()) {
            clear_screen();

            shooting->acknowledge(
                    receiving->process(
                        shooting->make_decision()));

            tmp = shooting;
            shooting = receiving;
            receiving = tmp;
        }
    } catch (const std::exception &e) {
        std::cout << e.what() << std::endl;
        return Game::Outcome::Abnormal;
    }

    return Game::Outcome::Finished;
}

