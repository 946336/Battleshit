#include "board.hpp"

#include "common.hpp"
#include <iostream>
#include <string>

#define ROW first
#define COL second

Board::Board(const Dims dims)
    : rows(dims.ROW), cols(dims.COL)
{
    /* cells = std::vector< std::vector<Cell> >(dims.ROW */
    /*         std::vector<Cell>(dims.COL), Cell(Open_Sea, 0)); */
    cells.reserve(dims.ROW);
    for (size_t i = 0; i < dims.ROW; ++i) {
        cells.push_back(
            std::vector< Cell > (dims.COL, Cell(Open_Sea, 0))
        );
    }

    nShips = 0;
}

Board::~Board()
{
    // Nothing to do here
}

bool Board::in_bounds(const Where where)
{
    if (where.ROW >= rows) return false;
    if (where.COL >= cols) return false;
    return true;
}

bool Board::place_ship(const Ship ship, const Where where,
        Facing facing)
{
    size_t ship_length = ship.length();

    if (!in_bounds(where)) {
        return false;
    }

    // Does the ship fit in that direction?
    switch (facing) {
        case Board::Facing::Up:
            if (where.ROW < ship_length - 1) return false;
            break;
        case Board::Facing::Right:
            if (where.COL > cols - ship_length) return false;
            break;
        case Board::Facing::Down:
            if (where.ROW > rows - ship_length) return false;
            break;
        case Board::Facing::Left:
            if (where.COL < ship_length - 1) return false;
            break;
    }

    // Figure out which cells the ship covers
    std::vector< std::pair< size_t, size_t > > cell_coords;
    switch (facing) {
        case Board::Facing::Up:
            for (size_t i = 0; i < ship_length; ++i) {
                cell_coords.push_back(
                        std::make_pair(where.ROW - i, where.COL)
                    );
            }
            break;
        case Board::Facing::Right:
            for (size_t i = 0; i < ship_length; ++i) {
                cell_coords.push_back(
                        std::make_pair(where.ROW, where.COL + i)
                    );
            }
            break;
        case Board::Facing::Down:
            for (size_t i = 0; i < ship_length; ++i) {
                cell_coords.push_back(
                        std::make_pair(where.ROW + i, where.COL)
                    );
            }
            break;
        case Board::Facing::Left:
            for (size_t i = 0; i < ship_length; ++i) {
                cell_coords.push_back(
                        std::make_pair(where.ROW, where.COL - i)
                    );
            }
            break;
    }

    // Make sure we don't place ships on top of each other
    for (size_t i = 0; i < cell_coords.size(); ++i) {
        auto coords = cell_coords[i];
        if (cells[coords.ROW][coords.COL].contents != &Open_Sea) {
            return false;
        }
    }

    ships[nShips++] = ship;

    // Attempt to place the ship
    for (size_t i = 0; i < cell_coords.size(); ++i) {
        auto coords = cell_coords[i];
        auto &row = cells[coords.ROW];
        row[coords.COL] = Cell(ships[nShips - 1], i);
    }

    return true;
}

Shot Board::shoot(const Where where)
{
    if (!in_bounds(where)) {
        throw WhereWasThatGoing(
              "Cannot shoot location ("
            + std::to_string(where.ROW)
            + ", "
            + std::to_string(where.COL)
            + ") on a Board with dimensions ("
            + std::to_string(rows)
            + ", "
            + std::to_string(cols)
            + ")."
        );
    }

    Ship::Damage res = cells[where.ROW][where.COL].shoot();

    return (res != Ship::Damage::Hit) ? Shot::Miss : Shot::Hit;
}

bool Board::has_unsunk_ships() const
{
    bool answer = false;
    for (const Ship &ship : ships) {
        if (ship.state() != Ship::State::Sunk) {
            answer = true;
        }
    }
    return answer;
}

void Board::print() const
{
    char label = 'A';
    char vis;
    std::cout << "  0 1 2 3 4 5 6 7 8 9" << std::endl;
    for (const auto &row : cells) {
        std::cout << label << ' ';
        for (const Cell &cell: row) {
            switch (cell.here()) {
                case Ship::Damage::Hit:
                    vis = 'X';
                    break;
                case Ship::Damage::Undamaged:
                    vis = cell.contents->ascii();
                    break;
            }
            std::cout << vis << ' ';
        }
        std::cout << label++ << ' ';
        std::cout << std::endl;
    }
    std::cout << "  0 1 2 3 4 5 6 7 8 9" << std::endl;
}


