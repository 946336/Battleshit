#include "cell.hpp"


Cell::Cell(Ship &ship, size_t sec)
    : contents(&ship), section(sec)
{
    // Nothing to do here
}

Cell::~Cell()
{
    // Nothing to do here
}

Ship::Damage Cell::shoot()
{
    return contents->take_damage(section);
}

Ship::Damage Cell::here() const
{
    return contents->part(section);
}

