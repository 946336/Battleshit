#ifndef BATTLESHIP_CELL_HPP
#define BATTLESHIP_CELL_HPP

#include "ship.hpp"
#include <memory>

struct Cell {
    public:
        Cell(Ship &ship, size_t sec);
        ~Cell();

        Ship *contents;
        size_t section;

        Ship::Damage shoot();
        Ship::Damage here() const;
};

#endif

