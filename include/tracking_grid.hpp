#ifndef BATTLESHIP_TRACKING_GRID_HPP
#define BATTLESHIP_TRACKING_GRID_HPP

#include <utility>
#include <vector>

#include "common.hpp"
#include "exceptions.hpp"
#include "result.hpp"

class Tracking_Grid {
    public:
        Tracking_Grid(const Dims dims = {10, 10});
        virtual ~Tracking_Grid();

        virtual void clear(const Where tl, const Where br);

        virtual void record(const Observation shot, const Where where);
        virtual void record(const Result result);

        virtual bool in_bounds(const Where where);

        virtual void print() const;

    protected:
        std::vector< std::vector< Observation > > cells;

        size_t rows;
        size_t cols;
};

#endif

