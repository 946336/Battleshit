#ifndef BATTLESHIP_SHIP_HPP
#define BATTLESHIP_SHIP_HPP

#include <vector>
#include <string>

#include "exceptions.hpp"

class Ship {
    public:
        Ship();
        Ship(const std::string name, const size_t length);
        Ship(const std::pair<std::string, size_t> ship_specs);
        virtual ~Ship();

        virtual Ship& operator=(const Ship &src);

        virtual bool operator==(const Ship &other);
        virtual bool operator!=(const Ship &other);

        using Place = size_t;
        enum State {Sunk = 0, Damaged, Pristine};
        enum class Damage {Hit = 0, Undamaged};

        virtual Damage take_damage(const Place where);

        State state() const;
        size_t length() const;
        Damage part(Place place) const;
        virtual char ascii() const;

        std::string title() const;
        size_t id() const;

    protected:
        std::string title_;
        size_t id_; // Unique by construction
        std::vector<Damage> sections;

};

// ====================================================================
// Required derived class: Open Sea
// ====================================================================

class Sea : public Ship {
    public:
        Sea(const std::string name, const size_t length)
            : Ship(name, length) {}
        using Place = size_t;

        virtual Damage take_damage(Place);
        virtual char ascii() const;
};

extern Sea Open_Sea;
// There should only be one. Furthermore, we should never construct another
// one.

#endif

