#ifndef BATTLESHIP_PLAYER_HPP
#define BATTLESHIP_PLAYER_HPP

#include <string>

#include "ship.hpp"
#include "turn.hpp"
#include "result.hpp"
#include "board.hpp"
#include "tracking_grid.hpp"

class Player {
    public:
        Player(const std::string name_);
        ~Player();

        virtual bool place_ship(const Ship ship);

        virtual Turn make_decision();

        virtual Result process(const Turn turn);

        virtual void acknowledge(const Result result);

        virtual bool has_lost() const;

        const std::string name;

        void show_board() const {
            board.print();
        }

    protected:
        Board board;
        Tracking_Grid tracking_grid;
};

#endif

