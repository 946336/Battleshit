#ifndef BATTLESHIP_BOARD_HPP
#define BATTLESHIP_BOARD_HPP

#include <utility>
#include <vector>
#include <array>

#include "cell.hpp"
#include "common.hpp"
#include "exceptions.hpp"

class Board {
    public:
        Board(const Dims dims = {10, 10});
        virtual ~Board();

        enum class Facing {Up = 0, Right, Down, Left};

        virtual bool place_ship(const Ship ship, const Where where,
                Facing facing);

        virtual Shot shoot(const Where where);

        virtual bool in_bounds(const Where where);
        virtual bool has_unsunk_ships() const;

        virtual void print() const;

    protected:
        std::vector< std::vector< Cell > > cells;

        Ship ships[5];
        size_t nShips = 0;

        size_t rows;
        size_t cols;
};

#endif

