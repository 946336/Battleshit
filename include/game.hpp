#ifndef BATTLESHIP_GAME_HPP
#define BATTLESHIP_GAME_HPP

#include <vector>
#include <memory>
#include <utility>

#include "player.hpp"

// This is a _very_ shitty driver for the game. Please don't ever use this.

class Game {
    public:
        Game();
        Game(Player player1);
        ~Game();

        enum class Join {Full = 0, Success};
        enum Outcome {Abnormal = 0, Finished};

        Join add(Player);

        Outcome play();

        std::vector< Player > losers() const;

        const size_t id;

    protected:
        const size_t MAX_PLAYERS = 2;
        const size_t MIN_PLAYERS = 2;
        std::vector<Player> players;

        static std::vector< std::pair<std::string, size_t> > ship_types;

        bool is_over() const;

        bool place_boats();
};

#endif

