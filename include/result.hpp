#ifndef BATTLESHIP_RESULT_HPP
#define BATTLESHIP_RESULT_HPP

#include "common.hpp"

#include "turn.hpp"

struct Result {
    public:
        Result(const Turn turn, const Shot shot)
            : instigator(turn), observation(
                    (shot == Shot::Hit) ? Observation::Hit : Observation::Miss
                ) {}
        ~Result() {}

        const Turn instigator;
        const Observation observation;
};

#endif

