#ifndef BATTLESHIP_TURN_HPP
#define BATTLESHIP_TURN_HPP

#include "common.hpp"

struct Turn {
    public:
        Turn(const Where where) : to_shoot(where) {}
        ~Turn() {}

        const Where to_shoot;
};

#endif

