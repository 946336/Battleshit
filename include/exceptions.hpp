#ifndef BATTLESHIP_EXCEPTIONS_HPP
#define BATTLESHIP_EXCEPTIONS_HPP

#include <stdexcept>
#include <string>

class WhereWasThatGoing : public std::runtime_error {
    public:
        WhereWasThatGoing(const std::string &what)
            : runtime_error(what) {}

        WhereWasThatGoing(const char *what)
            : runtime_error(what) {}
};


class WhoDoYouThinkIAm : public std::runtime_error {
    public:
        WhoDoYouThinkIAm(const std::string &what)
            : runtime_error(what) {}

        WhoDoYouThinkIAm(const char *what)
            : runtime_error(what) {}
};

class DontCountYourChickensBeforeTheyHatch : public std::runtime_error {
    public:
        DontCountYourChickensBeforeTheyHatch(const std::string &what)
            : runtime_error(what) {}

        DontCountYourChickensBeforeTheyHatch(const char *what)
            : runtime_error(what) {}
};

class NotSoFast : public std::runtime_error {
    public:
        NotSoFast(const std::string &what)
            : runtime_error(what) {}
        NotSoFast(const char *what)
            : runtime_error(what) {}
};

class ComeBackHereDammit : public std::runtime_error {
    public:
        ComeBackHereDammit(const std::string &what)
            : runtime_error(what) {}
        ComeBackHereDammit(const char *what)
            : runtime_error(what) {}
};

#endif

