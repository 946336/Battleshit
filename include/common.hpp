#ifndef BATTLESHIP_COMMON_HPP
#define BATTLESHIP_COMMON_HPP

#include <utility>
#include <string> // size_t

using Where = std::pair<size_t, size_t>;
using Dims  = std::pair<size_t, size_t>;

enum class Shot {Miss = 0, Hit};
enum class Observation {Miss = 0, Hit, Nothing};

#endif

