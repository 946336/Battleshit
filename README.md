# Questionable Battleship

Battleship, but shittier. You've been warned.

## Build Instructions

There may be submodules in this repository. As of this writing, they aren't
used and thus you can skip this step if you just don't feel like doing it.

For most of the world:

    git clone --recursive git@gitlab.com:946336/Battleshit.git

If you're using an older version (older than 1.6.5) of `git`:

    git clone git@gitlab.com:946336/Battleshit.git
    cd modules/log
    git submodule update --init --recursive

If you're just not feeling it today:

    git clone git@gitlab:946336/Battleshit.git

---

Have `cmake 3.0` or higher. You could also change the required `cmake` version
to whatever you have and it would probably still work.

You will also need a C++-11 compliant compiler, access to the C++ standard
library, and optionally access to a terminal emulator that recognizes VT100
escape sequences. Trust me, this last bit makes the whole experience that much
less unpleasant.

    cmake .
    make

This drops the `Battleshit` executable in `bin/`.

Set your expectations to nothing.

Play.

## Instructions, since I didn't put them in the actual game

Your name is 'Player 1' and your opponent's name is 'Player 2'. If you're the
second player, I'm talking to the other guy. You can't change your names.

When placing ships you will be asked for two pieces of information per ship:
It's position and its orientation.

Position is given in the traditional form of [row][col]. For example,

    A4

The letter must come first.

Ships can only align to the Cartesian axes, so your ships must face one of the
following directions:

* [U]p
* [R]ight
* [D]own
* [L]eft

Only the first letter matters.

The game is over when all of one player's ships have been sunk. Only the loser
is announced, because there are no winners in war.

